require 'fileutils'
require 'pathname'
require_relative 'lib/wrdashboard'

db_pathname     = Pathname('./.db')
public_pathname = Pathname('./public')
verbose_flag    = ENV['VERBOSE'] ? '--verbose' : '--quieter'

namespace :download do
  desc 'Download and compare geohub dataset list'
  task :geohub_dataset do
    WRDashboard.filter_geohub_dataset(
      Pathname('data/geohub.yml'),
      html_dir:     Pathname('public/'),
      download_dir: Pathname('.download/')
    )
  end

  desc 'Download data from the geohub'
  task :geohub do
    WRDashboard.build_geojson(
      Pathname('data/geohub.yml'),
      geojson_dir:  Pathname('public/map/'),
      html_dir:     Pathname('public/'),
      download_dir: Pathname('.download/')
    )
  end

  task all: %i[geohub geohub_dataset]
end

namespace :build do
  desc 'Build the index and other pages'
  task :pages do
    puts 'Build index pages'
    WRDashboard.render_hamls(
      ['source/index.html.haml', public_pathname.join('index.html')],
      ['source/map.html.haml', public_pathname.join('map', 'index.html')]
    )
  end

  desc 'Build the feed pages'
  task :feeds do
    puts 'Build feeds'

    WRDashboard.glob_pluto(
      './data/', 'update', db_pathname,
      verbose: ENV['VERBOSE']
    )
    WRDashboard.glob_pluto(
      './data/', 'merge', db_pathname,
      verbose: ENV['VERBOSE'],
      output:  public_pathname
    )
  end

  task all: %i[pages feeds]
end

desc 'Generate the feed pages without fetching'
task :merge do
  puts 'Build feeds'
  WRDashboard.glob_pluto(
    './data/', 'merge', db_pathname,
    verbose: ENV['VERBOSE'],
    output:  public_pathname
  )
end

desc 'Server the files build into the public directory'
task :server do
  require 'webrick'

  server = WEBrick::HTTPServer.new(
    Port:         4587,
    DocumentRoot: public_pathname.expand_path
  )
  puts "View the site at http://localhost:4587"
  trap('INT') { server.shutdown }
  server.start
end

task default: %w[download:all build:all]
