# TODO

## Sites which do not have feeds

Sites which do not have feeds which we can read, figure how to scrap them in the future.

### Articles

* [CBC Kitchener-Waterloo](http://www.cbc.ca/news/canada/kitchener-waterloo), [feed](https://www.cbc.ca/cmlink/rss-canada-kitchenerwaterloo)
* [Kitchener Citizen](http://www.kitchenercitizen.com)
* [Snapd - Kitchener-Waterloo](https://kitchenerwaterloo.snapd.com/)
* [Inside the Perimeter](https://insidetheperimeter.ca/)
* [Good Company Magazine](http://goodcomagazine.com/)
* [Aashni Shah](http://blog.aashni.me/)
* [Betakit Waterloo](https://betakit.com/tag/waterloo/)
* [Joseph Fung](http://www.josephfung.ca/)
* [Culture Fancier](http://www.culturefancier.com/)
* [Joe Kidwell](http://joekidwell.net/)
* [Charlie Drage](http://charliedrage.com/blog/)
* [Region of Waterloo New and Public Notices](https://www.regionofwaterloo.ca/Modules/News/search.aspx)
* [University of Waterloo Magazine](https://uwaterloo.ca/magazine/)
* [Jen Vasic](https://jenvasicwaterloo.ca/)
  - site does have RSS but it is empty
* [Chelsea Healthy Kitchen](http://chelseashealthykitchen.com/)
* [Emmett MacFarlane](http://www.emmettmacfarlane.com/)
* [Make it Kitchener](http://www.makeitkitchener.ca/)
* [Simon Pratt](http://blog.pr4tt.com/)
* [Kitchener Waterloo Community Foundation](https://www.kwcf.ca/)
* [Waterloo Region Greens](https://wrgreens.wordpress.com/)
* [Accelerator Centre](http://acceleratorcentre.com/)
* [Onion Honey](https://onionhoney.com/news--2)
* [Danny Michel](https://www.dannymichel.com/)
* [ENDURrun](https://www.endurrun.com/news/)
* [Belmont Village](http://thebelmontvillage.ca/news/)
* [Timeless Cafe and Bakery](http://www.timelesscafeandbakery.com/)
* [Diane Vernel's MPP Blog](https://us10.campaign-archive.com/home/?u=28b7d772a5bd2e30960f3004f&id=eec263041e)
* [KW Legacy](http://www.kwlegacy.ca/theprogram.php)
* [Cbrige.ca](https://cbridge.ca/)
* [Waterloo Stories](https://uwaterloo.ca/stories/)
* [Cooking with Linux](http://www.cookingwithlinux.com/)
* [Paulo Moncores](http://paulomoncores.com/)
* [The Cord](https://thecord.ca/)
* [CycleWR](http://cyclewr.ca/)
* [Lazaridis Institue](https://lazaridisinstitute.wlu.ca/news/index.html)
* [Tenille Bonogure](https://www.tenilleb.com/news)
* [Bob Jonkman](https://bobjonkman.ca/)
* [Jason Panda](http://kwpanda.com/)
* [Suzanne Church](http://suzannechurch.com/wordpress/)
* [Sarah Tolmie](http://sarahtolmie.ca/)
* [Grobo](https://www.grobo.io/pages/blog)
* [SydFit Health](https://sydfithealth.ca/blog-2/)
* [Connolly Design](http://www.connollydesign.com/)
* [Five Hundred Sparks](http://www.fivehundredsparks.ca/)
* [Waterloo County Rugby Club](https://www.waterloocountyrugby.com/news)
* [Waterloo Global Science Initiative(WGSI)](http://www.wgsi.org/blog)
* [Deutschmann Law](https://www.deutschmannlaw.com/blog/archive)
* [Prabhakar Ragde ](https://cs.uwaterloo.ca/~plragde/)
* [It's Me, Melis](https://www.itsmemelis.com/blog)
* [Tina Fletcher](http://tinafletcher.ca/)
* [The Armoury Clinic](https://thearmouryclinic.ca/blog/)
* [CuratedKW](https://curatedkw.ca/blog)
  - has [Atom](http://curatedkw.ca/blog/f.atom) but it does not work with Pluto 
* [WRDSB News](https://www.wrdsb.ca/)
* [UWaterloo Daily Bulletin](https://uwaterloo.ca/daily-bulletin/)
* [Wilfred Laurier University Student Union](https://yourstudentsunion.ca/)
* [Waterloo Undergraduate Student Association](https://wusa.ca/)
* [Luther Village On the Park](http://www.luthervillage.org/news)
* [Laurier News Hub](https://www.wlu.ca/news/index.html)
* [Laurier Centre for Sustainable Food Systems](https://researchcentres.wlu.ca/centre-for-sustainable-food-systems/news/index.html)
* [Waterloo Wind Energy Group](http://windenergy.uwaterloo.ca/)
* [Joe Martz](https://www.joemartz.com/blog)
* [GRT News](https://www.grt.ca/modules/news/search.aspx)
* [Jen Ziegler - Lifestyle Blog](https://jenziegler.com/lifestyle-blog/)
* [519 Sports Online](http://519sportsonline.ca/articles_news.php)
* [Makers Collective Blog](http://makerscollective.club/category/blog/)
* [A Different View Podcast](https://www.adifferentviewpodcast.com/blog)
* [Nick Matthews](https://www.nickmatthews.ca/)
* [WSI](https://www.thinkwsi.com/blog/)
* [Full Circle Foods](https://www.fullcirclefoods.ca/blog)
* [My Mother Named Me Sunshine](https://www.mymothernamedmesunshine.ca/)
* [Waterloo Stories](https://uwaterloo.ca/stories/)
* [Warm Embrace - Elder Care](https://warmembrace.ca/blog/)
* [Heart Beats Hate - Pulse](https://www.heartbeatshate.com/pulse)
* [Darwin AI](https://www.darwinai.com/news.html)
* [Waterloo Wellington LHIN](http://www.waterloowellingtonlhin.on.ca/newsandstories.aspx)
* [Wounds2Wings](https://www.wounds2wings.com/news--events.html), Nicole Brown
    Faulknor
* [Waterloo Region Small Business Centre](https://www.waterlooregionsmallbusiness.com/) newsletter
* [Danette Adams](https://danetteadams.com/blog/)
* [Daniel Botherston](https://danbrotherston.medium.com/)
* [JP Magic](https://www.jpmagic.ca/)
* [Jason Eckert](https://jasoneckert.github.io/)
* [Chuck Howitt](https://www.chuckhowitt.com/blog)
* [Alloy Casting](https://www.alloycasting.com/blog)
* [FranklyIT (Auvik)](https://www.auvik.com/franklyit/blog/)
* [Her Campus at Waterloo](https://www.hercampus.com/school/waterloo)
* [The Courtyard Kitchens](http://thecourtyardkitchens.ca/blog/)
* [WAConnect](https://www.waconnect.ca/)
* [Sunrise Cricket Club](http://sunrisecricketclub.com/)
* [David McPherson](https://davidmcpherson.ca/)

### Audio

* [Centre for International Governance Innovation](https://www.cigionline.org/multimedia#block-views-block-videos-block-5)
* https://soundcloud.com/todddonald
* [WCDSB: New Teacher Introduction Podcast!](https://sites.google.com/wcdsb.ca/wcdsb-ntip-podcast/home)
* [The Industry Podcast](https://www.instagram.com/the_industry_podcast/?hl=en)
* [Watershed Writers](https://watershedwriters.wordpress.com/)
* [My Best Friend is Gluten Free](https://www.mybfisgf.com/)
* [James Hobson/Hacksmith](https://stereo.com/hacksmith)
* [Michael Hitchens](https://soundcloud.com/michael-hitchens)

### Video

* [Centre for International Governance Innovation](https://www.cigionline.org/multimedia#block-views-block-videos-block-4)
* [Perimiter Intitue video archive](http://pirsa.org/)
* [Kitchener City Council meetings](https://www.kitchener.ca/en/city-services/watch-a-meeting.aspx)
* [Chris Inch](https://vimeo.com/chrisinch)
* [Woodgear](https://woodgears.ca/)

### Images
* https://www.instagram.com/matt.skw/
* https://www.matthewsmithphoto.net/
* https://www.instagram.com/richard_garvey123/
* https://www.instagram.com/starlightsocialclub/
* http://www.instagram.com/onionhoneymusic/
* https://www.instagram.com/jojoworthington/
* https://www.instagram.com/craftoberfestkw/
* https://www.instagram.com/theguitarcorner/
* https://www.instagram.com/conestogaconnected/
* https://www.instagram.com/robinmazumder/
* https://www.instagram.com/vintageblackcanada/
* https://www.instagram.com/mraaronfrancis/
* https://www.instagram.com/MATLOCKCORLEY/
* https://www.instagram.com/jyotigini/
* https://www.instagram.com/wlforiginals/
* https://www.instagram.com/weareprojectup/
* https://www.instagram.com/clementinecateringkw/
* https://www.instagram.com/whiteowlmystic/
* https://www.pinterest.ca/wrpssts/emergency-response-unit/
* https://www.instagram.com/designedbyraina/
* https://www.instagram.com/raina_bernard/
* https://www.instagram.com/trishaabe/
* https://www.instagram.com/dykebartakeoverkw/
* https://www.instagram.com/victoriaparkkitchener/
* https://www.instagram.com/colinboydshafer/
* https://www.instagram.com/rwartsfund/

### Facebook

* https://www.facebook.com/whiteowlmystic/
* https://www.facebook.com/ResonateWR/

## Repositories
* https://git.sr.ht/%7Esingpolyma/
* https://bitbucket.org/albertoconnor/

## Data Analysis and Charts
* https://public.tableau.com/profile/jeff.outhit#!/

## Unsorted Data and notes about data to find

* find the feeds for all the schools, universities and colleges in the region
* find sunshine list entries for the region
  - https://docs.google.com/document/d/1kSTnKfStYV5QW6noFR-G_5hxAz4J7GnUJhTjT4UA03A/edit
  - https://www.reddit.com/r/kitchener/comments/6x8ct2/city_of_kitchener_2016_sunshine_list_rankings_and/
* find data/feeds for AR Games
  - PokemonGo
  - Wizard's Unite
  - Ingress
* collect lists of march break/christmas/summer camps around the region
  - https://www.reddit.com/r/kitchener/comments/820ewm/march_break_day_camps/
  - https://horsebackadventures.ca/march-break-camp/
  - funworks
  - ymca
  - humane society
* https://juliewitmermaps.com/
* create diagrams of the municipal governments
  - would need to find all of these structures, including related agencies
  - how GRT works, including technology and organization
* https://twitter.com/WoodworthDDI
* https://twitter.com/iamquagmire
* https://twitter.com/TreehausCW

### Map/Geographic data

* find statscan data which covers the region
  - starting point for getting census data
  - https://www12.statcan.gc.ca/census-recensement/2016/geo/geosearch-georecherche/index-eng.cfm
* figure out how to collect incident/event kind of pages
  - how does the city report on "incidents"
  - water main break is an example
    * will there be a page where everything about it is collected
    * including the response from the construction companies and utility?
* [WalkScore](https://www.walkscore.com/CA-ON/Kitchener)
* [RedFin](https://www.redfin.ca/)
* [NextHome](https://nexthome.ca/)


## ICEBOX Unsorted project ideas

If they become more solid it might be worth considering making these into
issues.

* add a basic leaflet map, which is scoped to Waterloo Region
  - then can start adding things to it
  - investigate adding a timeline
    * https://github.com/hallahan/LeafletPlayback
  - sidebar
    * https://github.com/Turbo87/sidebar-v2
  - try to re-use the plugins which are used by OSM already
* add re-direct files for the short domains
* should add license notices
  - AGPLv3 for code
  - CC-BY-SA4.0 for documentation
  - ODbL for data?
    * https://wiki.openstreetmap.org/wiki/Open_Database_License
  - research
    * https://wiki.openmod-initiative.org/wiki/Choosing_a_license
    * https://en.wikipedia.org/wiki/Wikipedia:FAQ/Copyright#Can_I_add_something_to_Wikipedia_that_I_got_from_somewhere_else.3F
    * apparently CC-BY-SA4.0 is not compatiable with wikiepdia
      - https://meta.wikimedia.org/wiki/Terms_of_use/Creative_Commons_4.0/Legal_note
      - but they seem to be upgrading to CC-BY-SA4.0
    * https://meta.wikimedia.org/wiki/Terms_of_use/Creative_Commons_4.0/FAQ
    * https://en.wikipedia.org/wiki/Wikipedia:Copying_text_from_other_sources
    * https://en.wikipedia.org/wiki/Wikipedia:Copyrights
* add a CONTRIBUTORS file
* get HTTPS working
  - https://mkkhedawat.github.io/Enabling-HTTPS-for-Gitlab-pages-using-Certbot/
  - https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/
  - https://autonomic.guru/lets-encrypt-gitlab-again/
  - https://swas.io/blog/automatic-letsencrypt-gitlab-pages/
  - https://arothuis.nl/posts/lets-encrypt-gitlab-pages/
    * this one looks the most automated
    * using gitlab-le
  - https://mkkhedawat.github.io/Enabling-HTTPS-for-Gitlab-pages-using-Certbot/
  - https://github.com/rolodato/gitlab-letsencrypt
* consider writing a middleman-pluto extension
* document some uses cases
  - what restaurant is opening here?
    * https://twitter.com/m2bowman/status/1163928812251484160
  - pull all of uses cases from my presentation
* review the OpenStreetMap wiki and link Waterloo related things
  - https://wiki.openstreetmap.org/wiki/Waterloo_region
  - https://wiki.openstreetmap.org/wiki/Kitchener-Waterloo_mapping_party
  - https://www.bbbike.org/Waterloo/
  - https://wiki.openstreetmap.org/wiki/Kitchener-Waterloo
  - https://wiki.openstreetmap.org/wiki/Kitchener-Waterloo/Import
* wikipedia pages
  - https://en.wikipedia.org/wiki/James_Alan_Gardner
  - https://en.wikipedia.org/wiki/Allen_station_(Waterloo)
* create a list of use cases
  - bring in the examples that I used in my presentation
  - https://twitter.com/m2bowman/status/1163928812251484160
  - https://twitter.com/m2bowman/status/1168492401389948928
* research sources of unionization data
* old article aobut waterloo region which contains a lot stats
  - https://archive.macleans.ca/article/1940/7/15/kitchener-waterloo
* find datasets for outages in the region
  - power
  - water
  - sewage
  - gas
  - internet
  - telephone
* Create iCal and RSS aggregation for the region
  - collect existing feeds
  - scrape and create where necessary
  - geo-tag manually of from address information
* ical related research
  - [Location field in iCal](http://tools.ietf.org/html/rfc2445#page-84)
  - [Geo field in iCal](http://tools.ietf.org/html/rfc2445#section-4.8.1.6)
* categories to consider
  - artists
  - musicians
  - book authors
  - floss
  - ruby
  - javascript
  - twitter accounts
  - calendars
    * existing ical
    * and scrapping sites to generate ical
  - local 3d printing services
* general ideas
  - blogs
  - indieweb formatted things
  - calendar
  - video
  - audio
  - images
  - geographic
  - microblog
  - repositories/commits/pull requests
  - whatelse?
* weather
  - include historical
* astronomical events which are visible in the region
  - SAC has a database of objects from astronomers
  - @khalid has an article about this, find it
* create a framework for making microsites with WRDashboard data
  - https://humantransit.org/basics/the-transit-ridership-recipe#frequency
  - what do people need?
  - basically figure out a framework using
    * leaftlet
    * vis.js
    * and the scripts for processing OpenData
  - this might be a good way to help people start with things they are interested in
  - then also publish the list of ideas that I have
  - let other people pick them up
* investigate creating a more dashboard-y kind of view
  - get rid of the bodies of the articles
  - consider a dashboard showing activity of entities
    * [Fraidycat](https://fraidyc.at/) is a good example
