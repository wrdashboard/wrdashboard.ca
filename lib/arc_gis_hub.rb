# frozen_string_literal: true

require 'cgi'
require 'csv'
require 'zip'

# Interface for the ArcGIS Hub Open Data Portal
#
# @see https://www.esri.com/en-us/arcgis/products/arcgis-hub/overview
# @see https://www.w3.org/TR/vocab-dcat-2/#Class:Catalog
class ArcGISHub
  # @private
  class CachedDownloader
    # @param download_dir [Pathname, String]
    def initialize(download_dir)
      @download_dir = Pathname(download_dir)
    end

    # @param url [String]
    #
    # @return [Pathname] downloaded filename
    def download(url)
      puts "Download #{url}"
      system(
        [
          'wget',
          '--quiet',
          '--compress=auto',
          '--timestamping',
          "--directory-prefix=#{@download_dir}",
          "\"#{url}\""
        ].join(' ')
      )
      @download_dir.join(File.basename(CGI.unescape(url)))
    end

    # @param url [String]
    #
    # @return [Hash]
    def download_json(url)
      SugarUtils::File.read_json(download(url))
    end

    # @param url [String]
    #
    # @return [CSV::Table]
    def download_csv(url)
      CSV.table(download(url), header_converters: nil)
    end
  end

  # @see https://www.w3.org/TR/vocab-dcat-2/#Class:Dataset
  class Dataset
    # @param metadata [Hash]
    # @param downlaoder [ArcGISHub::CachedDownloader]
    #
    # @return [ArcGISHub::Dataset]
    def initialize(metadata, downloader)
      @metadata   = metadata
      @downloader = downloader
    end

    # @return [String]
    def title
      @metadata['title']
    end

    # @return [nil]
    # @return [String]
    def source
      @metadata.dig('publisher', 'source') || @metadata.dig('publisher', 'name')
    end

    # @return [String]
    def label
      "#{title} (#{source})"
    end

    # @return [String]
    def html_url
      @metadata['landingPage']
    end

    # @return [String]
    def description
      @metadata['description']
    end

    # @return [Array<String>]
    def media_types
      @metadata['distribution'].map { |x| x['mediaType'] }
    end

    # @return [Boolean]
    def geojson?
      media_types.any? { |x| x =~ /geo\+json/ }
    end

    # @return [String]
    # @return [Integer]
    # @return [Array]
    # @return [Hash]
    def [](key)
      @metadata[key]
    end

    # @return [Hash]
    def to_h
      @metadata
    end

    # @return [Array<Hash>]
    def geojson_features
      geojson_distribution =
        @metadata['distribution'].find { |x| x['mediaType'] =~ /geo\+json/ }
      fail("#{label} does not have GeoJSON") unless geojson_distribution

      @downloader.download_json(geojson_distribution['accessURL'])['features']
    end

    # @return [Boolean]
    def csv?
      media_types.any? { |x| x == 'text/csv' }
    end

    # @overload csv
    # @overload csv
    #   @param exclude_by_headers [Array]
    # @return [CSV::Table]
    def csv(*exclude_by_headers)
      csv_distribution =
        @metadata['distribution'].find { |x| x['mediaType'] == 'text/csv' }
      fail("#{label} does not have CSV") unless csv_distribution

      csv = @downloader.download_csv(csv_distribution['accessURL'])

      headers = exclude_by_headers.flatten.compact
      csv.delete(*headers) unless headers.empty?

      csv
    end

    # @return [Hash{String => CSV::Table}]
    def multi_csv
      distribution =
        @metadata['distribution'].find { |x| x['mediaType'] == 'application/json' }

      fail("#{label} does not have multi-CSV") unless distribution
      access_url = distribution['accessURL']
      fail("#{label} does not have multi-CSV zip") unless access_url =~ /\.zip$/

      zip_pathname       = @downloader.download(access_url)
      directory_pathname =
        zip_pathname.dirname + zip_pathname.basename.sub(zip_pathname.extname, '')
      directory_pathname.rmtree if directory_pathname.directory?
      directory_pathname.mkpath
      Zip::File.open(zip_pathname) do |zip_file|
        zip_file.each do |entry|
          next if entry.name =~ /Facilities/
          entry.extract(directory_pathname.join(entry.name))
        end
      end

      result = {}
      directory_pathname.glob('**/*.csv') do |pathname|
        result[pathname.basename.to_s] =
          CSV.table(
            pathname,
            header_converters: nil,
            encoding:          'windows-1251:utf-8'
          )
      end
      result
    end
  end

  # @param catalog_url [String]
  # @param download_dir [String, Pathname]
  def initialize(catalog_url, download_dir)
    @catalog_url = catalog_url
    @downloader  = CachedDownloader.new(download_dir)
  end

  # @param url [String]
  #
  # @return [ArcGISHub::DataSet]
  def dataset(url)
    catalog.find do |dataset|
      normalize_url(url) == normalize_url(dataset.html_url)
    end
  end

  # @param urls [Array<String>]
  #
  # @return [Array<ArcGISHub::DataSet>]
  def datasets_at(urls)
    normalized_urls = urls.map { |x| normalize_url(x) }

    catalog.select do |dataset|
      normalized_urls.include?(normalize_url(dataset.html_url))
    end
  end

  # @param urls [Array<String>]
  #
  # @return [Array<ArcGISHub::DataSet>]
  def datasets_filter_at(urls)
    normalized_urls = urls.map { |x| normalize_url(x) }

    catalog.reject do |dataset|
      normalized_urls.include?(normalize_url(dataset.html_url))
    end
  end

  # @return [Array<ArcGISHub::DataSet>]
  def catalog
    @catalog ||=
      @downloader
      .download_json(@catalog_url)['dataset']
      .map { |x| Dataset.new(x, @downloader) }
  end

  ##############################################################################

  private

  # Remove the HTTP(S) schema for the URL to that we can compare use the
  # address and path.
  #
  # @param url [STring]
  #
  # @return [String]
  def normalize_url(url)
    url.delete_prefix('https://').delete_prefix('http://').sub(/_[0-9]+$/, '')
  end
end
