# frozen_string_literal: true

require 'forwardable'
require 'date'
require 'oj'
require 'sugar_utils'
require 'yaml'
require_relative '../arc_gis_hub'

class WRDashboard
  class ArcGISHubConfig
    class Collection
      attr_reader :type
      attr_reader :arc_gis_hub

      # @param type [String]
      # @param data [Hash]
      # @param arc_gis_Hub [ArcGISHub]
      #
      # @return [ArcGISHubConfig::Collection]
      def initialize(type, data, arc_gis_hub)
        @type        = type
        @data        = data
        @arc_gis_hub = arc_gis_hub
      end

      # @return [String]
      def title
        @data['title']
      end

      # @return [String]
      def icon
        @data['icon']
      end

      # @return [Boolean]
      def marker?
        !icon.nil?
      end

      # @return [Array<ArcGISHubConfig::Datasets>]
      def datasets
        @data['datasets'].map { |dataset| Dataset.new(dataset, self) }
      end
    end

    class Dataset
      extend Forwardable
      def_delegator :@collection, :type
      def_delegator :@collection, :title
      def_delegator :@collection, :marker?

      # @param data [Hash]
      # @param collection [ArcGISHub::Collection]
      #
      # @return [ArcGISHubConfig::Dataset]
      def initialize(data, collection)
        @data        = data
        @collection  = collection
      end

      # @return [String]
      def label
        @collection.arc_gis_hub.dataset(@data['url']).label
      end

      # @return [Hash]
      def geojson_features
        @collection.arc_gis_hub.dataset(@data['url']).geojson_features
      end

      # @param feature [Hash]
      #
      # @return [String]
      def geojson_feature_id(feature)
        feature['properties'][@data['id_property_key']]
      end

      # @param feature [Hash]
      #
      # @return [Array<OpenStruct{#header,#caption,#csv}>]
      def join_data(feature)
        return [] unless @data.key?('join_data')

        join_key = @data['join_data']['id_property_key'] || @data['id_property_key']
        return [] unless join_key

        feature_id = geojson_feature_id(feature)

        if @data['join_data']['table']
          table = @data['join_data']['table']

          @join_data_csv ||=
            @collection
            .arc_gis_hub
            .dataset(@data['join_data']['url'])
            .multi_csv

          result = []
          @join_data_csv
            .select { |filename, _csv| filename.include?(table) }
            .each_pair do |_filename, csv|
              selected_join_data_csv =
                CSV::Table.new(
                  csv.select { |row| row[join_key] == feature_id },
                  headers: csv.headers
                )
              selected_join_data_csv.delete(join_key)

              result.push(
                OpenStruct.new(
                  header:  table,
                  caption: nil,
                  csv:     selected_join_data_csv
                )
              )
            end

          if @data['join_data']['join_data']
            table    = @data['join_data']['join_data']['table']
            join_key = @data['join_data']['join_data']['id_property_key']

            key_ids = result.first.csv.values_at(join_key).flatten

            @join_data_csv
              .select { |filename, _csv| filename.include?(table) }
              .each_pair do |_filename, csv|
                selected_join_data_csv =
                  CSV::Table.new(
                    csv.select { |row| key_ids.include?(row[join_key]) },
                    headers: csv.headers
                  )
                selected_join_data_csv.delete(join_key)

                result.push(
                  OpenStruct.new(
                    header:  table,
                    caption: nil,
                    csv:     selected_join_data_csv
                  )
                )
              end
          end

          result
        else
          @join_data_csv ||=
            @collection
            .arc_gis_hub
            .dataset(@data['join_data']['url'])
            .csv(
              "\xEF\xBB\xBFOBJECTID",
              'OBJECTID',
              @data['join_data']['exclude_keys']
            )

          selected_join_data_csv =
            CSV::Table.new(
              @join_data_csv.select { |row| row[join_key] == feature_id },
              headers: @join_data_csv.headers
            )
          selected_join_data_csv.delete(join_key)

          [
            OpenStruct.new(
              header:  nil,
              caption: nil,
              csv:     selected_join_data_csv
            )
          ]
        end
      end

      # @param feature [Hash]
      #
      # @return [Integer]
      def geojson_feature_year(feature)
        if @data['year_property_key']
          feature['properties'][@data['year_property_key']].to_i
        elsif feature['properties'][@data['start_date_property_key']]
          date_value = feature['properties'][@data['start_date_property_key']]
          if date_value.is_a?(String)
            if date_value =~ /^[0-9]{8}$/
              # Assume the first 4 digits are the year.
              date_value.slice(0, 4).to_i
            else
              Date.parse(date_value).year
            end
          else
            0
          end
        else
          0
        end
      end

      # @param feature [Hash]
      #
      # @return [Boolean]
      def geojson_feature_exclude?(feature)
        return false unless @data['exclude_by_property']

        if @data['exclude_by_property'].is_a?(Hash)
          @data['exclude_by_property'].each do |property_key, value|
            return true if feature['properties'][property_key] == value
          end
        elsif @data['exclude_by_property'].is_a?(Array)
          fail('exclude_by_property Array not yet supported')
        end

        false
      end
    end

    # @param config_filename [String, Pathname]
    # @param download_dir [String, Pathname]
    #
    # @return [WRDashboard::ArcGISHubConfig]
    def initialize(config_filename, download_dir)
      @config_filename = Pathname(config_filename)
      @download_dir    = Pathname(download_dir)
    end

    # @param options [Hash]
    # @option options [Boolean] :include_skipped
    #
    # @return [Array<ArcGISHub::Collection>]
    def map_marker_collections(options = {})
      return @map_markers_by_type if @map_markers_by_type

      result = []

      collections =
        if options[:include_skipped]
          nested_config('map_markers', Hash)
        else
          nested_config('map_markers', Hash).reject { |k, v| v['skip'] }
        end
      collections.each do |type, content|
        result.push(Collection.new(type, content, arc_gis_hub))
      end

      result
    end

    # @return [Array<ArcGISHub::DataSet>]
    def map_marker_datasets
      @map_marker_datasets ||= arc_gis_hub.datasets_at(map_marker_identifiers)
    end

    # @return [Array<ArcGISHub::DataSet>]
    def overlay_datasets
      @overlay_datasets ||= arc_gis_hub.datasets_at(overlay_identifiers)
    end

    # @return [Array<ArcGISHub::DataSet>]
    def openstreetmap_import_datasets
      @openstreetmap_import_datasets ||=
        arc_gis_hub.datasets_at(openstreetmap_import_identifiers)
    end

    # @return [Array<ArcGISHub::DataSet>]
    def rejected_datasets
      @rejected_datasets ||= arc_gis_hub.datasets_at(rejected_identifiers)
    end

    # @return [Array<ArcGISHub::DataSet>]
    def unfiltered_geojson_datasets
      @unfiltered_geojson_datasets ||=
        arc_gis_hub.datasets_filter_at(all_identifiers).select(&:geojson?)
    end

    # @return [Array<ArcGISHub::DataSet>]
    def unfiltered_other_datasets
      @unfiltered_other_datasets ||=
        arc_gis_hub.datasets_filter_at(all_identifiers).reject(&:geojson?)
    end

    ############################################################################

    private

    # @return [Hash]
    def config_data
      @config_data ||= YAML.load_file(@config_filename) || {}
    end

    def nested_config(key, type)
      if config_data[key].is_a?(String)
        filename = @config_filename.dirname.join(config_data[key])
        YAML.load_file(filename) || type.new
      else
        config_data[key] || type.new
      end
    end

    def map_marker_identifiers
      @map_marker_identifiers ||=
        nested_config('map_markers', Hash)
        .values
        .map { |x| x['datasets'] }
        .flatten
        .map { |x| [x['url'], x.dig('join_data', 'url')] }
        .flatten
        .compact
    end

    def overlay_identifiers
      @overlay_identifers ||=
        nested_config('overlays', Hash)
        .values
        .map { |x| x['datasets'] }
        .flatten
        .map { |x| x['url'] }
    end

    def openstreetmap_import_identifiers
      @openstreetmap_import_identifiers ||=
        nested_config('openstreetmap_imports', Array).map { |x| x['url'] }
    end

    def rejected_identifiers
      @rejected_identifiers ||=
        nested_config('rejects', Array).map { |x| x['url'] }
    end

    def all_identifiers
      [
        map_marker_identifiers,
        overlay_identifiers,
        openstreetmap_import_identifiers,
        rejected_identifiers
      ].flatten
    end

    # @return [ArcGISHub]
    def arc_gis_hub
      @arc_gis_hub ||= ArcGISHub.new(config_data['url'], @download_dir)
    end
  end
end
